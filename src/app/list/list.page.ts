import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { CommonService } from '../services/common.service';

const { Storage } = Plugins;

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  listArray: Array<any> = [];
  constructor(private activatedRoute: ActivatedRoute,public commonService : CommonService) { }

  ngOnInit() {
    this.getObject();
    this.commonService.detectLocalStorageChange().subscribe(async resp => {
      console.log(resp);
      if(resp === 'success') {
        this.getObject();
      }
    });
  }

  async getObject() {
    const ret = await Storage.get({ key: 'formData' });
    const user = JSON.parse(ret.value);
    if(user )
    {
      console.log(user);
      this.listArray = [...user];
      console.log("list",this.listArray);
    }
  
  }


}
