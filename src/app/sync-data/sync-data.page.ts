import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-sync-data',
  templateUrl: './sync-data.page.html',
  styleUrls: ['./sync-data.page.scss'],
})
export class SyncDataPage implements OnInit {
  paryList: any =[];
  actionList: any=[];
  constructor(private commonService: CommonService,) { }

  ngOnInit() {

  }

  syncData() {
    this.commonService.getPartyBodyData().subscribe((resp: any) => {
      if (resp && resp.data) {
        this.paryList = resp.data;
        
        this.commonService.setLocalStorageData('PartyData',this.paryList);
        console.log(resp)
      }

    });

    this.commonService.getCurrent_call_type_followupactionBody().subscribe((resp: any) => {
      if (resp && resp.FOLLOWUP) {
        this.actionList = resp.FOLLOWUP;
        this.commonService.setLocalStorageData('ActionData',resp);
        console.log(resp)
      }
    });
  }



}
