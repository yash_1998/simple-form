import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError, delay } from 'rxjs/operators';
import { forkJoin, Observable, Subject, throwError } from 'rxjs';
import { Plugins } from '@capacitor/core';
import { ToastController } from '@ionic/angular';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  formDataArray: Array<any> = [];
  combineApiCalls = [];
  userData = [] as any;
  changeLocalStorageData = new Subject<any>();
  constructor(public http: HttpClient, public toastController: ToastController) { }
  toast: any;

  async getObject() {
    this.combineApiCalls = [];
    const ret = await Storage.get({ key: 'formData' });
    this.userData = JSON.parse(ret.value);
    if (this.userData && this.userData.length > 0) {
      this.formDataArray = [...this.userData];
      for (let i = 0; i < this.formDataArray.length; i++) {
        this.combineApiCalls.push(this.getBodyData(this.formDataArray[i]))
      }
      this.sendPostRequest(this.combineApiCalls);
    }
  }

  setLocalStorageDetect(data) {
    this.changeLocalStorageData.next(data);
  }

  detectLocalStorageChange(): Observable<any> {
    return this.changeLocalStorageData.asObservable();
  }

  getFormData(data) {
    console.log(data, "data");
    const formData = new FormData();
    for (let dataKey in data) {
      formData.append(dataKey, data[dataKey]);
    }
    console.log(formData);
    return formData;
  }


  getBodyData(data) {
    let postData = {
      "CNSTR": "MTgyLjE4LjE0NC4yNy9vcmNsMTFAI0BTQVBAVlNPT0QjQDY3NEAh",
      "USR": "UVRTQCNAU0FQQFZTT09EI0A2NzRAIQ==",
      "IMGPATH": "",
      "bgrp": "",
      "1612508575566": "",
      "LLUSR": "TEST1",
      "COMPNM": "QUICK TOUR SAPPHIRE",
      "LATI": "31.319056",
      "LNG": "75.5743006",
      "OPT": "N",
      "PKGMODE": "REGULAR",
      "AREA": "",
      "DIST": "",
      "DEVICEDT": "05 - FEB - 2021",
      "DT": "05 - FEB - 2021",
      "PRODUCT": "",
      "PAYMENT": "CASH",
      "RECEIPT_TYPE": "",
      "RECEIPT_START_NO": "undefined",
      "RECEIPT_NO": "",
      "AMOUNT": "",
      "shwproduct": "N",
      "shwfollowdt": "Y",
      "shwfollowaction": "Y",
      "shwpayment": "N",
      "shwamount": "N",
      "imgtyp": "",
      "typ": "N",
      "manager_inst": "",
      "diffmtr": "10"
    }
    let formValue = { ...postData, ...data };
    return this.getApiCalls(this.getFormData(formValue));
  }

  getPartyBodyData() {
    let postData = {
      "CNSTR": "MTgyLjE4LjE0NC4yNy9vcmNsMTFAI0BTQVBAVlNPT0QjQDY3NEAh",
      "USR": "UVRTQCNAU0FQQFZTT09EI0A2NzRAIQ==",
      "IMGPATH": "",
      "bgrp": "",
      "1612760786242": "",
      "AGENT": "TEST1",
      "FIX_PARTY": "N",
      "COMDET_ALLOW": "N"
    }
    return this.getPartyList(this.getFormData(postData));
  }

  getCurrent_call_type_followupactionBody() {
    let postData = {
      "CNSTR": "MTgyLjE4LjE0NC4yNy9vcmNsMTFAI0BTQVBAVlNPT0QjQDY3NEAh",
      "USR": "UVRTQCNAU0FQQFZTT09EI0A2NzRAIQ==",
      "IMGPATH": "",
      "bgrp": "",
      "1612760786242": ""
    }
    return this.getgetCurrent_call_type_followupaction(this.getFormData(postData));
  }


  sendPostRequest(data) {
    forkJoin(data).pipe(delay(300)).subscribe(resp => {
      const changeArray = [];
      const successArray = [];
      for (let i = 0; i < resp.length; i++) {
        let storedValue: any = resp[i];
        if (storedValue.status === "Success") {
          successArray.push(this.userData[i]);
        } else {
          this.userData[i].error = JSON.stringify(storedValue);
          changeArray.push(this.userData[i]);
        }
        let msg = `Syncing  to server ${successArray.length} of ${resp.length}, do not closing app`
        this.presentToast(msg);
        setTimeout(() => {
          this.presentToast("Sync successfully");
        }, 5000);
      }
      // this.dismissToast();
      this.setLocalStorageData('formData', changeArray);
      setTimeout(() => {
        this.setLocalStorageDetect('success');
      }, 1000);

    });
    // this.saveData()
    //   .subscribe(resp => {
    //     console.log(resp);
    //     if (resp.status === "Success") {

    //       for (let i = 0; i < localStorage.length; i++) {
    //         let storedValue = localStorage.key(i);
    //         console.log(`Item at ${i}: ${storedValue}`);
    //       }

    //     }
    //     console.log(resp);
    //   }, error => {
    //     console.log(error);
    //   });
  }

  saveData(data) {
    return this.http.post("https://noproxy.in/qlead/api/visitat_receipt.php", data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  getNewPartyList(data) {
    return this.http.post("https://noproxy.in/qlead/api/devparty_maxsno.php", data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }


  getApiCalls(data) {
    return this.http.post("https://noproxy.in/qlead/api/visitat_receipt.php", data)
  }

  getgetCurrent_call_type_followupaction(data) {
    return this.http.post("https://noproxy.in/qlead/api/markVstList.php", data)
  }

  getPartyList(data) {
    return this.http.post("https://noproxy.in/qlead/api/devparty.php", data)
  }

  errorHandler(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error || 'server error.');
  }


  async presentToast(message) {
    this.toast = await this.toastController.create({
      message,
      duration: 5000
    })
    this.toast.present();
  }


  // dismissToast() {
  //   if(this.toast) {
  //     this.toast.dismiss();
  //   }
  // }

  async setLocalStorageData(key, dataArray) {
    await Storage.set({
      key,
      value: JSON.stringify(
        dataArray
      )
    });
  }


}
