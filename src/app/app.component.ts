import { Component, OnInit } from '@angular/core';
import { Network, NetworkStatus, PluginListenerHandle } from '@capacitor/core';
import { Plugins } from '@capacitor/core';
import { CommonService } from './services/common.service';
const { Storage } = Plugins;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  networkStatus: NetworkStatus;
  networkListener: PluginListenerHandle;
  public appPages = [
    { title: 'Form', url: '/folder/Form', icon: 'mail' },
    { title: 'List', url: '../list', icon: 'paper-plane' },
    { title: 'Sync', url: '../sync-data', icon: 'paper-plane' },
  ];
  partyList: any = [];

  constructor(public commonService: CommonService,
  ) { }


  async ngOnInit() {
    let status = await Network.getStatus();
    if (status.connected) {
      this.commonService.getObject();
      const body = {
        CNSTR: 'MTgyLjE4LjE0NC4yNy9vcmNsMTFAI0BTQVBAVlNPT0QjQDY3NEAh',
        USR: 'UVRTQCNAU0FQQFZTT09EI0A2NzRAIQ==',
        IMGPATH: '',
        bgrp: '',
        1612847891201: '',
        AGENT: 'TEST1',
        FIX_PARTY: 'N',
        COMDET_ALLOW: 'N',
        maxcode: '1834',
      }

      const partyData: any = await Storage.get({ key: 'PartyData' });
      if (partyData) {
        this.partyList = JSON.parse(partyData.value);
        if (this.partyList && this.partyList.length > 0) {
          console.log(this.partyList);
          // const changeArray = [...this.partyList];
          // const maxValueArray = changeArray.sort((a, b) => b.CODE - a.CODE);
          let maxValue = this.partyList.reduce((prev, current) => {
            return (parseInt(prev.CODE) > parseInt(current.CODE) ? prev : current);
          });
          // let maxValue;
          if (maxValue && maxValue.CODE) {
            body.maxcode = maxValue.CODE;
          }
          this.commonService.getNewPartyList(this.commonService.getFormData(body)).subscribe(async resp => {
            console.log(resp);
            if (resp && resp.data.length > 0) {
              this.partyList.push(resp.data[0]);
              this.partyList.sort((a, b) => a.PARTY.toLowerCase().localeCompare(b.PARTY.toLowerCase()));
              this.commonService.setLocalStorageData('PartyData', this.partyList);
            }
          });
        }
      }
    }
    this.networkListener = Network.addListener('networkStatusChange', (status) => {
      console.log("Network status changed", status);
      this.networkStatus = status;
      if (this.networkStatus.connected) {
        this.commonService.getObject();
      }
    });
    this.commonService.detectLocalStorageChange().subscribe(async resp => {
      if (resp !== 'success') {
        let status = await Network.getStatus();
        console.log(status);
        if (status.connected) {
          this.commonService.getObject();
        }
      }
    });

  }

  // showOnceToast() {
  //   this.toastController.dismiss().then((obj) => {
  //   }).catch(() => {
  //   }).finally(() => {
  //     this.presentToast();
  //   });
  // }





  ngOnDestroy() {
    this.networkListener.remove();
  }
} 
