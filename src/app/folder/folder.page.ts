import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
import { Ng2ImgMaxService } from 'ng2-img-max';
import { FormBuilder } from '@angular/forms';
import { CommonService } from '../services/common.service';

const resizebase64 = require('resize-base64');




@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  @ViewChild('filePicker', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;
  userForm: any;
  selectedFile: File;
  submitted = false;
  image: any;
  isSaved: any = false;
  formDataArray: Array<any> = [];
  dataArray: Array<any> = [];
  partyList: any = [];
  actionList: any = [];

  createForm() {
    this.userForm = this.formBuilder.group({
      PARTY: [''],
      REMARKS: [''],
      Image: [''],
      FOLLOWDT: [''],
      FOLLOWACTION: [''],
      currentcalltype: [''],
      IMG: ['']
    });
  }

  constructor(
    private commonService: CommonService,
    private ng2ImgMaxService: Ng2ImgMaxService,
    private activatedRoute: ActivatedRoute,
    public platform: Platform,
    private formBuilder: FormBuilder) { }


  async ngOnInit() {
    this.createForm();
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
    this.getObject();
    this.commonService.detectLocalStorageChange().subscribe(async resp => {
      console.log(resp);
      if (resp === 'success') {
        this.getObject();
      }
    });
    this.getLocalStorageData();
  }

  async getObject() {
    const ret = await Storage.get({ key: 'formData' });
    if (ret && ret.value) {
      this.formDataArray = JSON.parse(ret.value);
      console.log(this.formDataArray);
    }
  }

  onFileChoose(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.ng2ImgMaxService.resizeImage(file, 100, 100).subscribe(
      result => {
        console.log(result);
        const pattern = /image-*/;
        const reader = new FileReader();

        if (file && !file.type.match(pattern)) {
          console.log('File format not supported');
          return;
        }

        reader.onload = () => {
          this.image = reader.result.toString();
          this.userForm.controls['IMG'].setValue(this.image);
          // const img = resizebase64(this.image, 100, 100);

          console.log("resize", this.image);
          // this.userForm.controls['IMG'].setValue(resizebase64(this.image , 100 , 100));
        };
        reader.readAsDataURL(result);
      },
      error => {
        console.error('Error: ', error);
      }
    );
    // this.userForm.controls['IMG'].setValue(JSON.stringify(file));
    // const pattern = /image-*/;
    // const reader = new FileReader();

    // if (file && !file.type.match(pattern)) {
    //   console.log('File format not supported');
    //   return;
    // }

    // reader.onload = () => {
    //   this.image = reader.result.toString();
    //   const img = resizebase64(this.image, 100, 100);

    //   console.log("resize",img);
    //   // this.userForm.controls['IMG'].setValue(resizebase64(this.image , 100 , 100));
    // };
    // reader.readAsDataURL(file);
  }

  async getLocalStorageData() {
    const partyData = await Storage.get({ key: 'PartyData' });
    if (partyData) {
      this.partyList = JSON.parse(partyData.value);
    }


    var actionData = await Storage.get({ key: 'ActionData' });
    if (actionData) {
      this.actionList = JSON.parse(actionData.value);
    }
  }


  onSubmit() {
    this.submitted = true;
    // if (this.userForm.invalid) {
    //   return;
    // }
    delete this.userForm.value.Image;
    let formValue = { ...this.userForm.value };
    this.formDataArray.push(formValue);
    console.log("fordta", this.formDataArray);
    this.setObject(this.formDataArray);
    this.userForm.reset();
  }

  async setObject(dataArray) {
    await Storage.set({
      key: 'formData',
      value: JSON.stringify(
        dataArray
      )
    });
    this.commonService.setLocalStorageDetect(dataArray);
  }

}
